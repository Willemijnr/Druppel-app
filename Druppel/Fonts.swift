//
//  Fonts.swift
//  Druppel
//
//  Created by Willemijn Reibestein on 31-05-16.
//  Copyright © 2016 Willemijn Reibestein. All rights reserved.
//

let FONT_SFUITEXT_REGULAR = "SF-UI-Text-Regular"
let FONT_SFUITEXT_BOLD = "SF-UI-Text-Bold"
let FONT_SFUITEXT_LIGHT = "SF-UI-Text-Light"
