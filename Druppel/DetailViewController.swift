//
//  DetailViewController.swift
//  Druppel
//
//  Created by Willemijn Reibestein on 20-06-16.
//  Copyright © 2016 Willemijn Reibestein. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, NRFManagerDelegate {

    // Labels
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var stepsLabel: UILabel!
    @IBOutlet weak var literLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    // Properties
    var steps:Int = 0
    var temp:Double = 0.0
    let dateFormatter = NSDateFormatter()
    
    // Bottle / water
    var nrfManager:NRFManager!
    let arduino:String = "UART"
    var connected:Bool = false
    
    let capacity:Double = 440.00
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.dateFormat = "dd-MM-YYYY HH:mm"

        nrfManager = NRFManager(delegate:self)

        tempLabel.text = String(format:"%.1f", temp).stringByReplacingOccurrencesOfString(".", withString: ",")
        stepsLabel.text = String(steps)
    }
    
    // Connect or disconnect the bottle, changing button title
    @IBAction func reconnect(sender: UIButton) {
        if !connected {
            nrfManager.connect(arduino)
            sender.setTitle("Disconnect your bottle", forState: .Normal)
        } else if connected {
            nrfManager.disconnect()
            sender.setTitle("Connect your bottle", forState: .Normal)
        }
    }
    
    // Send a notification to the bottle, for getting data
    @IBAction func refresh(sender: AnyObject) {
        self.nrfManager.writeString("1")
    }
    
    /** NRFManagerDelegate methods **/
    
    func nrfDidConnect(nrfManager:NRFManager)
    {
        print("Connected with \(nrfManager.showName())")
        statusLabel.text = "Connected"
        connected = true
    }
    
    func nrfDidDisconnect(nrfManager:NRFManager)
    {
        print("Disconnected")
        statusLabel.text = "Disconnected"
        connected = false
    }
    
    func nrfReceivedData(nrfManager:NRFManager, data: NSData?, string: String?) {
        print("Received data - String: \(string!)")
        let value = Double(string!)! * 4.6
        print(value)
        literLabel.text = String(self.capacity - value).stringByReplacingOccurrencesOfString(".", withString: ",")
        statusLabel.text = "Synced \(dateFormatter.stringFromDate(NSDate()))"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
