//
//  ViewController.swift
//  Druppel
//
//  Created by Willemijn Reibestein on 31-05-16.
//  Copyright © 2016 Willemijn Reibestein. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    // Temperature
    let locationManager:CLLocationManager = CLLocationManager()
    var weather:WeatherHelper = WeatherHelper()
    var temperature:Double = 0
    
    // Steps
    var healthKit:HealthKitHelper = HealthKitHelper()
    var steps:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationAuthorization()
        healthKitAuthorization()
    }
    
    // Start updating location
    func locationAuthorization() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    // Did Update locations
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let value = locations[locations.count - 1].coordinate
        
        if temperature == 0 {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                self.weather.getTemperature(value.latitude, long:value.longitude) {
                    temp, error in
                    dispatch_async(dispatch_get_main_queue()) {
                        self.temperature = temp
                        print("Temperature: \(temp)")
                    }
                }
            }
        }
    }
    // Start getting total steps
    func healthKitAuthorization() {
        if healthKit.checkAutorization() {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                self.healthKit.getTotalSteps() { steps, error in
                    dispatch_async(dispatch_get_main_queue()) {
                        self.steps = steps
                        print("Total steps: \(steps)")
                    }
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Send steps and temperature to other view, DetailViewController
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showData" {
            let detailViewController = segue.destinationViewController as! DetailViewController
            detailViewController.steps = self.steps
            detailViewController.temp = self.temperature
        }
    }
}

