//
//  MainButtonView.swift
//  Druppel
//
//  Created by Willemijn Reibestein on 31-05-16.
//  Copyright © 2016 Willemijn Reibestein. All rights reserved.
//

import UIKit

class MainButtonView: UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.titleLabel?.font = UIFont(name: FONT_SFUITEXT_LIGHT, size: 18)
        self.titleLabel?.numberOfLines = 0;
        self.layer.cornerRadius = 2;
        self.layer.backgroundColor = UIColor(hexString: COLOR_BLUE)?.CGColor
        
    }
}
