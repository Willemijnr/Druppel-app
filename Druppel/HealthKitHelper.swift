//
//  HealthKitHelper.swift
//  Druppel
//
//  Created by Willemijn Reibestein on 20-06-16.
//  Copyright © 2016 Willemijn Reibestein. All rights reserved.
//

import UIKit
import HealthKit

class HealthKitHelper: NSObject {

    private let _storage: HKHealthStore? = {
        if HKHealthStore.isHealthDataAvailable() {
            return HKHealthStore()
        } else {
            return nil
        }
    }()
    
    var totalSteps:Int = 0
    
    func checkAutorization() -> Bool {
        var isEnabled:Bool = true;
        let steps = NSSet(object: HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)!)
        
        if HKHealthStore.isHealthDataAvailable() {
            _storage?.requestAuthorizationToShareTypes(nil, readTypes: steps as? Set<HKObjectType>) {
                (success, error) -> Void in
                isEnabled = success
                
                if success {
                    //print("Successfull Autorization")
                } else {
                    print("Error: \(error)")
                }
            }
        } else {
            isEnabled = false;
        }
        
        return isEnabled
    }
    
    func getTotalSteps(completion: (Int, NSError?) -> () ) {
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Year, .Month, .Day], fromDate: NSDate())
        
        guard let startDate = calendar.dateFromComponents(components) else {
            fatalError("*** Unable to create the start date ***")
        }
        
        let endDate = calendar.dateByAddingUnit(.Day, value: 1, toDate: startDate, options: [])
        let type = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)
        let predicate = HKQuery.predicateForSamplesWithStartDate(startDate, endDate: endDate, options: .None)
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd MMMM - HH:mm"
        
        let query = HKSampleQuery(sampleType: type!, predicate: predicate, limit: Int(HKObjectQueryNoLimit), sortDescriptors: nil) {
            (query, results, error) in
            
            if results?.count > 0 {
                for result in results as! [HKQuantitySample] {
                    //let date = dateFormatter.stringFromDate(result.startDate)
                    let steps = Int(result.quantity.doubleValueForUnit(HKUnit.countUnit()))
                    //print("Steps \(steps) - \(date)")
                    self.totalSteps += steps
                }
                //print("Total steps:\(self.totalSteps)")
            }
            completion(self.totalSteps, error)
        }
        _storage!.executeQuery(query)
    }
}
