//
//  Weather.swift
//  Druppel
//
//  Created by Willemijn Reibestein on 20-06-16.
//  Copyright © 2016 Willemijn Reibestein. All rights reserved.
//

import UIKit

class WeatherHelper: NSObject {
    
    private let _url = "http://api.openweathermap.org/data/2.5/weather"
    private let _APIKey = "af8512662ee231ecaee17662691c4263"
    
    override init() {
        super.init()
        
    }
    
    func getTemperature(lat:Double, long:Double, completion:(Double, NSError?)->()) {
        let session = NSURLSession.sharedSession()
        let requestURL = NSURL(string: "\(self._url)?APPID=\(self._APIKey)&lat=\(lat)&lon=\(long)")!
        
        let dataTask = session.dataTaskWithURL(requestURL) {
            (data, repsonse, error) in
            if (error != nil) {
                print("Error: \(error)")
            } else {
                completion(self.getTemp(data), error)
            }
        }
        
        dataTask.resume()
    }
    
    private func getTemp(data: NSData?) -> Double {
        do {
            if let data = data, response = try NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions(rawValue:0)) as? [String: AnyObject] {
                let kelvin = response["main"]!["temp"]
                let celsius = (kelvin as? Double)! - 273.15
                
                return celsius
            } else {
                print("JSON ERROR")
            }
        } catch let error as NSError {
            print("Error parsing results: \(error.localizedDescription)")
        }
        
        return 0.0
    }
    
    
}
